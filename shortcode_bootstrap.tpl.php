<ol class = "visualshortcodes visualshortcodes-parent<?php print $live_preview; ?>">
  <div class = "visualshortcodes-settings-links visualshortcodes-main-add">
    <i class="fa fa-plus-circle visualshortcodes_add pull-right"></i>
  </div>
  <div class = "clearfix visualshortcodes-remove"></div>
  <?php print $output; ?>
  <div class = "clearfix visualshortcodes-remove end-layout"></div>
  <div class = "visualshortcodes-settings-links visualshortcodes-main-add bottom-links">
    <i class="fa fa-plus-circle visualshortcodes_add pull-right"></i>
  </div>
  <div class = "clearfix visualshortcodes-remove"></div>
</ol>
