This module only supports theme bootstrap 3 administration.
Before installing Bootstrap Shortcode layout editor, it must install shortcode fontawgie module
- Add a new format for content text admin/config/content/format
- enable the Shortcodes Bootstrap Editor option
- Activate some desired functions in the optional filter
- Create new node, selection new format in body type filter option , 
you will see a button that launches the bootstrap editor, click (+) at right side to add new element
- (optional) all libraies js used with cdn but you can download and include / all / library websites
- See details in shortcode_option.inc file
