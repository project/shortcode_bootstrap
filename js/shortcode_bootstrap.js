(function() {
  var $ = jQuery;
  $.fn.removeClassPrefix = function(prefix) {
    this.each(function(i, el) {
      var classes = el.className.split(" ").filter(function(c) {
        return c.lastIndexOf(prefix, 0) !== 0;
      });
      el.className = classes.join(" ");
    });
    return this;
  };

  Drupal.behaviors.rotate_blocks = {
    attach: function (context, settings) {
      if(!navigator.userAgent.match(/iPad|iPhone|Android/i)) {
        $(".product, .employee", context).hover(function(event) {
          event.preventDefault();
          $(this).addClass("hover");
        }, function(event) {
          event.preventDefault();
          $(this).removeClass("hover");
        });
      }
    }
  };

  Drupal.behaviors.href_click = {
    attach: function (context, settings) {
      $("a[href='#']").click(function() {
        return false;
      });
    }
  };

  Drupal.behaviors.attachSelectBox = {
    attach: function (context, settings) {
      if(typeof($.fn.selectBox) !== "undefined") {
        $("select:not(.without-styles)").selectBox();
      }
    }
  };

  Drupal.behaviors.removefromcart = {
    attach: function (context, settings) {
      // Remove from block Cart
      $(".cart-header .product-remove:not(.ajax-processed)").once("ajax").click(function() {
        $.post($(this).attr("href"));
        $(this).parents("li").animate({"opacity": 0, "height" : 0}, 700, function() {
          $(this).remove();
          $(".cart-count").text(parseInt($(".cart-header .cart-count").text()) - 1);
        });
        return false;
      });
      // Click on the button from styled icon on the Cart Page
      $(".button-click:not(.click-processed)").once("click").click(function() {
        $(this).prev("input").click();
        return false;
      });
    }
  };

  Drupal.behaviors.removefromcompare = {
    attach: function (context, settings) {
      // Remove from block Cart
      $("#compare-table .product-remove:not(.ajax-processed)", context).once("ajax").click(function() {
        $.post($(this).attr("href"));
        $("#compare-table tr .data-index-" + $(this).attr("data-index")).animate({"opacity": 0, "height" : 0}, 700, function() {
          $(this).remove();
        });
        $(".compare-header .flag-counter").text(parseInt($(".compare-header .flag-counter").text()) - 1);
        return false;
      });
    }
  };

  Drupal.behaviors.contextual_form = {
    attach: function (context, settings) {
      // Stop the handler of contextual links to close the popup
      $(".contextual-form:not(.contextual-form-processed)", context).once("contextual-form").click(function(e) {
        e.stopPropagation();
      });
    }
  };

  Drupal.behaviors.livicons = {
    attach: function (context, settings) {
      if(typeof($.fn.updateLivicon) !== "undefined") {
        $(".livicon:not(.livicon-processed)", context).once("livicon").updateLivicon();
      }
    }
  };

  Drupal.behaviors.animation = {
    attach: function (context, settings) {
      var parent, child, scrollWidth;
      if (scrollWidth === undefined) {
        parent = $("<div style='width: 50px; height: 50px; overflow: auto'><div/></div>").appendTo("body");
        child = parent.children();
        scrollWidth = child.innerWidth() - child.height(99).innerWidth();
        parent.remove();
      }
      $("[data-appear-animation]").each(function() {
        var $this = $(this);
        $this.addClass("appear-animation");
        if($("body").width() + scrollWidth > 767) {
          $this.appear(function() {
            var delay = ($this.data("appear-animation-delay") ? $this.data("appear-animation-delay") : 1);
            if(delay > 1) {
              $this.css("animation-delay", delay + "ms");
            }
            $this.addClass($this.data("appear-animation"));
            setTimeout(function(){
              $this.addClass("appear-animation-visible");
            }, delay);
          }, {accX: 0, accY: -150});
        } else {
          $this.addClass("appear-animation-visible");
        }
      });
    }
  };

  /* Animation Progress Bars */
  Drupal.behaviors.progressBar = {
    attach: function (context, settings) {
      $("[data-appear-progress-animation]").each(function() {
        var $this = $(this);
        $this.appear(function() {
          var delay = ($this.data("appear-animation-delay") ? $this.data('appear-animation-delay') : 1);

          if(delay > 1) {
            $this.css("animation-delay", delay + "ms");
          }

          $this.find(".progress-bar").addClass($this.data("appear-animation"));

          setTimeout(function() {
            $this.find(".progress-bar").animate({
              width: $this.attr("data-appear-progress-animation")
            }, 500, "easeInCirc", function() {
              $this.find(".progress-bar").animate({
                textIndent: 10
              }, 1500, "easeOutBounce");
            });
          }, delay);
        }, {accX: 0, accY: -50});
      });

      // Circular Bars - Knob

      $(".knob").each(function () {
        var $this = $(this),
          knobVal = $this.attr("rel");

        $this.knob({
          "draw" : function () {
            $(this.i).val(this.cv + "%");
          }
        });

        $this.appear(function() {
          $({
            value: 0
          }).animate({
            value: knobVal
          }, {
            duration : 2000,
            easing   : "swing",
            step     : function () {
              $this.val(Math.ceil(this.value)).trigger("change");
            }
          });
        }, {accX: 0, accY: -150});
      });

    }
  };

  Drupal.behaviors.pixastic = {
    attach: function (context, settings) {
      if ($(".fwb-blur").length) {
        var blurBox = $(".fwb-blur");

        blurBox.each(function () {
          var $this = $(this),
            img     = new Image(),
            amount  = 2,
            prependBox = '<div class="blur-box"></div>';

          img.src = $this.data("blur-image");

          if (
            $this.data("blur-amount") !== undefined &&
            $this.data("blur-amount") !== false
          ){
            amount = $this.data("blur-amount");
          }

          img.onload = function() {
            $(this).pixastic(
              "blurfast",
              {amount:amount},
              function(){
                $(".blur-page").addClass("blur-load");
              });
          };

          $this.prepend( prependBox ).find(".blur-box").prepend( img );
        });
      }
    }
  };
  //player youtube background
  Drupal.behaviors.YTPlayer = {
    attach: function (context, settings) {
      if($(".player").length) {
        $(".player").YTPlayer();
      }
    }
  };

}());
