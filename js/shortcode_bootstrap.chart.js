(function ($) {
  $(document).ready(function () {

    function isDate(sDate) {
      if(sDate.toString() == parseInt(sDate).toString()) return false;
      var tryDate = new Date(sDate);
      return (tryDate && tryDate.toString() != "NaN" && tryDate != "Invalid Date");
    }
    function convertTableIntoArray(tbl) {
      var tblData = "";
      var tblArr = new Array();
      tblLength = document.getElementById(tbl).rows.length;
      for (let i = 0; i < tblLength; i++) {
        tblData = "";
        for (j = 0; j < document.getElementById(tbl).rows[i].cells.length; j++) {
          tblData += document.getElementById(tbl).rows[i].cells[j].innerHTML + ",";
        }
        tblData = tblData.substring(0, tblData.length - 1)
        tblArr[i] = tblData.split(",");
        tblArr[i].forEach(function (value, index) {
          value = $.trim(value.replace(" ","").replace(",",".").replace("|LF|","{").replace("|RF|","}"));
          if($.isNumeric(value)){
            if(value.match(/^-{0,1}\d+$/)){
              tblArr[i][index] = parseInt(value);
            }else if(value.match(/^\d+\.\d+$/)){
              tblArr[i][index] = parseFloat(value);
            }
          }else if(value.indexOf("{")>-1){
            tblArr[i][index] = JSON.parse(value);
          }else if(isDate(value)){
            tblArr[i][index] = new Date(value);
          }
        });
      }
      return tblArr;
    }

    var packages = ["corechart"];
    $(".chart").each(function () {
      let type = false;
      if ($(this).data("type") !== undefined && $(this).data("type") !== false) {
        type = $(this).data("type");
      }
      if(type === "Table" ){
        packages.unshift("table");
      }
      if(type === "TreeMap" ){
        packages.unshift("treemap");
      }
      if(type === "WordTree" ){
        packages.unshift("wordtree");
      }
      if(type === "Timeline" ){
        packages.unshift("timeline");
      }
      if(type === "Sankey" ){
        packages.unshift("sankey");
      }
      if(type === "OrgChart" ){
        packages.unshift("orgchart");
      }
      if(type === "Map" ){
        packages.unshift("map");
      }
      if(type === "Gauge" ){
        packages.unshift("gauge");
      }
      if(type === "Gantt" ){
        packages.unshift("gantt");
      }
      if(type === "AnnotationChart" ){
        packages.unshift("annotationchart");
      }
      if(type === "Calendar" ){
        packages.unshift("calendar");
      }
    });
    // Load Charts and the corechart and barchart packages.
    google.charts.load("current", {"packages": packages});
    // Draw the pie chart and bar chart when Charts is loaded.
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
      $(".chart").each(function () {
        var
          packages = "corechart",
          $this = $(this),
          id = $this.attr("id"),
          title = "",
          type  = "LineChart",
          width = "100%",
          height  = "225",
          stacked = false,
          is3D  = false,
          legend_position = "right",
          pieHole = ""
        ;
        if ($this.data("type") !== undefined && $this.data("type") !== false) {
          type = $this.data("type");
        }
        if ($this.data("height") !== undefined && $this.data("height") !== false) {
          height = $this.data("height");
        }
        if ($this.data("width") !== undefined && $this.data("width") !== false) {
          width = $this.data("line-width");
        }
        if ($this.data("stacked") !== undefined && $this.data("stacked") !== false) {
          stacked = $this.data("stacked");
        }
        if ($this.data("legend_position") !== undefined && $this.data("legend_position") !== false) {
          legend_position = $this.data("legend_position");
        }
        if ($this.data("3d") !== undefined && $this.data("3d") !== false) {
          is3D = $this.data("3d");
        }
        if ($this.data("piehole") !== undefined && $this.data("piehole") !== false) {
          pieHole = $this.data("piehole");
        }
        if ($this.find("caption") !== undefined && $this.find("caption") !== false) {
          title = $this.find("caption").text();
        }

        //set option

        var options = {
          title: title,
          curveType: "function",
          height : height,
          legend: { position: legend_position }
        };
        if($.isNumeric(width)){
          options.width = width;
        }
        if($.isNumeric(pieHole)){
          options.pieHole = pieHole;
        }
        if(is3D){
          options.is3D = true;
        }

        var dataTable = convertTableIntoArray(id);
        // get all variables so we can remove table
        $this.parent().attr("id",id).html("");

        //set data for line

        if(type === 'Calendar'){
          var data = new google.visualization.DataTable();
          data.addColumn({ type: 'date', id: 'Date' });
          data.addColumn({ type: 'number', id: 'Won/Loss' });
          data.addRows(dataTable);
        }else if(type === 'Sankey'){
          var data = new google.visualization.DataTable();
          data.addColumn('string', 'From');
          data.addColumn('string', 'To');
          data.addColumn('number', 'Weight');
          data.addRows(dataTable);
        }else if(type === 'CandlestickChart'){
          options = {
            legend:'none'
          };
          var data = new google.visualization.arrayToDataTable(dataTable,true);
        }else {
          var data = new google.visualization.arrayToDataTable(dataTable);
        }

        if(type === "ComboChart"){
          options.seriesType = 'bars';
        }
        var chart = new google.visualization[type](document.getElementById(id));
        //draw chart
        chart.draw(data, options);
      });
    }
  });
})(jQuery);
