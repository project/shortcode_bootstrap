(function ($) {

  var cache_shortcode = [];
  var global_counter = 0;

  $.fn.outerHTML = function () {
    return $("<div/>").append(this.eq(0).clone()).html();
  };

  $.fn.removeClassPrefix = function (prefix) {
    this.each(function (i, el) {
      var classes = el.className.split(" ").filter(function (c) {
        return c.lastIndexOf(prefix, 0) !== 0;
      });
      el.className = classes.join(" ");
    });
    return this;
  };
  if(typeof $.ui !== "undefined"){
    orig_allowInteraction = $.ui.dialog.prototype._allowInteraction;
    $.ui.dialog.prototype._allowInteraction = function (event) {
      if ($(event.target).closest(".cke_dialog").length) {
        return true;
      }
      return orig_allowInteraction.apply(this, arguments);
    };
  }

  function visualshortcodes_add_overlay() {
    $("body").append("<div class = 'visualshortcodes-overlay'><i class='fa fa-refresh fa-spin'></i></div>");
  }

  function visualshortcodes_remove_overlay() {
    $(".visualshortcodes-overlay").remove();
  }

  /**
   * Ajax delivery command to open shortcode settings form
   */
  Drupal.ajax.prototype.commands.shortcode_settings = function (ajax, response, status) {
    if (response.data) {
      $("#visualshortcodes-shortcode-settings").html(response.data);
      $("#visualshortcodes-shortcode-settings").dialog("open");
    }
    visualshortcodes_remove_overlay();
  };

  Drupal.behaviors.visualshortcodes_delete_saved = {
    attach: function (context, settings) {
      $(".delete-saved-shortcode", context).click(function () {
        var id = $(this).parent().find("select").val();
        $(this).parent().find("select option[value='" + id + "']").remove();
        $.post(Drupal.settings.basePath + "ajax/visualshortcodes/ajax_backend_delete_saved", {"id": id});
        return false;
      });
    }
  }

  Drupal.behaviors.shortcode_bootstrap = {
    attach: function (context, settings) {

      $(".visualshortcodes_links:not(.default-hide-processed)", context).once("default-hide").hide();

      // Load Visual Shortcodes layout
      $(".visualshortcodes_links:not(.visual-shortcodes-processed)", context).once("visual-shortcodes").click(function () {

        if (!Drupal.settings.shortcode_bootstrap.formats[$(this).data("format")]) {
          return;
        }
        // Remove the old Layout
        $("#" + $(this).data("id")).prev(".visualshortcodes").remove();
        // Upload Visual Shortcodes if Enable text clicked
        if ($.trim($(this).text()) == $.trim($(this).data("enable-text"))) {
          var shortcode = $("#" + $(this).data("id")).val();
          $(this).parent().find("> .fa-spin").show();
          $this = $(this);
          $.ajax({
            async: false,
            type: "POST",
            url: Drupal.settings.basePath + "ajax/visualshortcodes/ajax_backend_layout",
            data: {
              code: shortcode,
              format: $(this).data("format")
            },
            dataType: "html",
            success: function (layout) {
              $("#" + $this.data("id")).before(layout);
              $this.parent().find("> .fa-spin").hide();
              visualshortcodes_link_text_update($this);
              Drupal.attachBehaviors($("#" + $this.data("id")).prev(), Drupal.settings);
            }
          });
        } else {
          visualshortcodes_link_text_update($(this));
        }
        return false;
      });

      // Shortcode Copy
      $(".visualshortcodes-copy:not(.visualshortcodes-processed)", context).once("visualshortcodes").click(function () {
        var clone = $(this).closest(".shortcode-processed").outerHTML();
        $(this).closest(".shortcode-processed").after(clone);
        $(this).closest(".shortcode-processed").next().find(".visualshortcodes-processed").removeClass("visualshortcodes-processed");
        Drupal.attachBehaviors($(this).closest(".shortcode-processed").parent(), Drupal.settings);
        visualshortcodes_save($(this).closest(".visualshortcodes").next());
      });

      function visualshortcodes_collect_attrs($this) {
        var attributes = $this.length ? $this.prop("attributes") : {};
        var attrs_array = {};
        $.each(attributes, function () {
          if (this.name != "shortcode") {
            // Convert "{${" to "[", "}$}" to "]", "|{" to "<" and "|}" to ">"
            attrs_array[this.name] = this.value.replace(/\|LS\|/g, "[").replace(/\|RS\|/g, "]").replace(/\|LF\|/g, "{").replace(/\|RF\|/g, "}").replace(/\|\{/g, "<").replace(/\|\}/g, ">");
          }
        });
        return attrs_array;
      }

      // Show the Shortcode Settings Form
      $(".shortcode-settings:not(.visualshortcodes-processed)", context).once("visualshortcodes").click(function () {
        visualshortcodes_add_overlay();
        $this = $(this).closest(".shortcode-processed");
        $(this).parents(".visualshortcodes").addClass("active_layout");
        $(".shortcode-opened-settings").removeClass("shortcode-opened-settings");
        // Mark shortcode which opened the Settings Form
        $this.addClass("shortcode-opened-settings");
        var attrs_array = visualshortcodes_collect_attrs($this);
        var html = $this.find(".hidden:first").html();
        if (typeof (html) == "string") {
          // Allow to add "[" and "]" to HTML shortcode
          html = html.replace(/\|LS\|/g, "[").replace(/\|RS\|/g, "]").replace(/\|LF\|/g, "{").replace(/\|RF\|/g, "}");
        }
        // Load the Settings form via default Drupal AJAX request
        var ajax = new Drupal.ajax(false, "#doesnt-matter", {url: Drupal.settings.basePath + "admin/ajax/visualshortcodes/ajax_backend_shortcode"});
        ajax.beforeSerialize = function (element_settings, options) {
          options["data"]["shortcode"] = $this.attr("shortcode");
          options["data"]["attrs"] = attrs_array;
          options["data"]["text"] = html;
          return options;
        };
        // Run ajax request
        ajax.eventResponse(ajax, {});
      });

      // Add new shortcode form
      $(".visualshortcodes-save:not(.visualshortcodes-processed)", context).once("visualshortcodes").click(function () {
        visualshortcodes_save_form($(this));
      });

      // Add new shortcode form
      $(".visualshortcodes_add:not(.visualshortcodes-processed)", context).once("visualshortcodes").click(function () {
        visualshortcodes_add_form($(this));
      });

      $(".visualshortcodes_enabled_links:not(.visualshortcodes-processed) button", context).once("visualshortcodes").click(function () {
        $(".visualshortcodes_enabled_links .active").removeClass("active");
        $(this).addClass("active");
        return false;
      });

      // Search on the Add Shortcode Form
      $(".visualshortcodes_enabled_list_search input:not(.visualshortcodes-processed)", context).once("visualshortcodes").keyup(function () {
        visualshortcodes_add_form_search($(this));
      });
      $(".visualshortcodes_enabled_list_search input", context).keyup();

      // Update Visual Shortcode status upon Textarea format is changed
      $(".filter-list:not(.visual-shortcodes-format-processed)", context).once("visual-shortcodes-format").change(function () {

        // Disable visual shortcodes if they enabled because the text format is changed
        var text_link = $(this).parents(".text-format-wrapper").find(".visualshortcodes_links:first").text(),
          text_disable_link = $(this).parents(".text-format-wrapper").find(".visualshortcodes_links").data("disable-text"),
          format_link = $(this).parents(".text-format-wrapper").find(".visualshortcodes_links").data("format");
        if (text_link == text_disable_link && format_link != $(this).val()) {
          $(this).parents(".text-format-wrapper").find(".visualshortcodes_links").click();
        }
        if (typeof (Drupal.settings.shortcode_bootstrap.formats) !== undefined && typeof (Drupal.settings.shortcode_bootstrap.formats[$(this).val()]) != undefined && Drupal.settings.shortcode_bootstrap.formats[$(this).val()]) {
          // Setup the format for easy use then Layout will be rendered
          $(this).parents(".text-format-wrapper").find(".visualshortcodes_links").data("format", $(this).val());
          $(this).parents(".text-format-wrapper").find(".visualshortcodes_links, .visualshortcodes").show();
        } else {
          $(this).parents(".text-format-wrapper").find(".visualshortcodes_links, .visualshortcodes").hide();
          $(this).parents(".text-format-wrapper").find(".form-textarea").show();
        }

      });

      // Update all Visual Shortcode link status
      visualshortcodes_link_text_update($(".visualshortcodes_links", context));

      // Autostart
      $(".visualshortcodes_links:not(.autostart-processed)", context).once("autostart").click();

      // Sortable Layout
      $(".visualshortcodes:not(.sort-processed)").once("sort").sortable({
        containerSelector: ".visualshortcodes-parent",
        containerPath: "> .border-style",
        delay: 200,
        onDrop: function ($item, container, _super, event) {
          $item.removeClass("dragged").removeAttr("style");
          $("body").removeClass("dragging");
          visualshortcodes_save($item.parents(".visualshortcodes").next());
        }
      });

      $(".visualshortcodes-parent").disableSelection();

      // Setup correct classes for COL shortcode
      $("[shortcode='col']", context).each(function () {
        $(this).removeClassPrefix("col");
        var classes = {"phone": "xs", "tablet": "sm", "desktop": "md", "wide": "lg"};
        for (i in classes) {
          if ($(this).attr(i)) {
            $(this).addClass("col-" + classes[i] + "-" + $(this).attr(i));
          }
        }
      });
      //replace with type = color
      //$(".colorpicker-enable:not(.color-processed)", context).once("color").colorpicker();

    } // End of attach behaviour
  }; // End behaviour


  // Handle the Media Upload
  Drupal.behaviors.visualshortcodes_media_upload = {
    attach: function (context, settings) {

      $(".media-upload:not(.media-processed)", context).once("media").each(function () {
        $(this).click(function () {
          visualshortcodes_upload_image($(this));
          return false;
        });
        // Hide Delete button if there is no image
        if (!$(this).closest(".form-item").find("input").val()) {
          $(this).parent().find(".media-remove").hide();
        }
      });

      $(".media-remove:not(.media-processed)", context).once("media").click(function () {
        visualshortcodes_remove_image($(this));
        return false;
      });

    } // End of attach behaviour
  }; // End behaviour

  /**
   * Save Visual Shortcodes to textarea value
   */
  function visualshortcodes_save($textarea) {
    var shortcode = "<div class = 'hidden visualshortcodes_save_render'>" + $textarea.prev(".visualshortcodes").html() + "</div>";
    $("body").append(shortcode);
    // Process NoChilds tag, remove 'hidden' wrapped tag
    $(".visualshortcodes_save_render .border-style > .hidden").each(function () {
      //$(this).parent().html($(this).html().replace(/\{\$\{/g, "[").replace(/\}\$\}/g, "]"));
      $(this).parent().html($(this).html().replace(/\[/g, "|LS|").replace(/\]/g, "|RS|").replace(/\{/g, "|LF|").replace(/\}/g, "|RF|"));
    });

    // Remove styling tags
    $(".visualshortcodes_save_render").find(".border-style").each(function () {
      _visualshortcodes_remove_borders($(this));
    });
    $(".visualshortcodes_save_render").find(".visualshortcodes-settings-links, .visualshortcodes-remove, .backend_preview").remove();

    $(".visualshortcodes_save_render").find(".shortcode-processed").removeClass("shortcode-processed visualshortcodes-parent-wrap row shortcode-opened-settings visualshortcodes-sortable visualshortcodes-parent sort-processed ui-sortable").removeClassPrefix("col").removeClassPrefix("visualshortcodes");
    // Replace tag name with shortcode name
    $(".visualshortcodes_save_render > [shortcode!='']").each(function () {
      prepare_to_shortcodes($(this));
    });
    var html = $(".visualshortcodes_save_render").html().replace(/(\r\n|\n|\r)/gm, "");
    html.replace(/(<(vsb_([^0-9]+)[0-9]*))([^>]*)>(.*)(<\/\2>)/g, "[$3$4]\n$5\n[/$3]\n", "g");
    html.replace("\u2028", "\\u2028").replace("\u2029", "\\u2029");
    while (html.indexOf("<vsb_") >= 0) {
      html = html.replace(/(<(vsb_([^0-9]+)[0-9]*))([^>]*)>(.*)(<\/\2>)/g, "\n[$3$4]$5[/$3]\n", "g");
    }
    html = html.replace(/[ ]+/g, " ").replace(/^([^\S]*)/, "").replace(/\]([^\S\[]*)\[/g, "]\n[").replace(/[\n]+/g, "\n");
    $textarea.val(html);
    $(".visualshortcodes_save_render").remove();
    return false;
  }


  function _visualshortcodes_remove_borders($this) {
    $this.find(".border-style").each(function () {
      _visualshortcodes_remove_borders($(this));
    });
    $this.parent().html($this.html());
  }

  function _shortcode_form_to_settings($target_form) {
    $target_form.submit();
    settings = {};
    $target_form.find(":input").each(function () {
      if ($(this).attr("type") == "hidden" && !$(this).hasClass(".form-textarea") && !$(this).hasClass(".hidden")) {
        return;
      }
      var value = "";
      if ($(this).attr("type") == "checkbox") {
        value = $(this).is(":checked") ? 1 : 0;
      } else if ($(this).attr("type") == "radio") {
        value = $(this).closest(".form-radios").find("input[type='radio']:checked").val();
      } else {
        value = $(this).val();
      }
      settings[$(this).attr("name")] = value;
    });
    return settings;
  }

  function _settings_to_shortcode_attrs(settings, $shortcode) {
    for (var i in settings) {
      // Process HTML shortcode text value 
      if (i.indexOf("text_") == 0 && i.indexOf("[value]") > -1) {
        // Allow to add "[" and "]" to HTML shortcode
        if(i == "text_data[value]"){
          //use character specical § for replace break line in table data
          settings[i] = settings[i].replace(/(\r\n|\n|\r)/gm, "§");
        }
        $shortcode.find("h3").next().html(settings[i].replace(/\[/g, "|LS|").replace(/\]/g, "|RS|").replace(/\{/g, "|LF|").replace(/\}/g, "|RF|"));
      } else if (i.indexOf("text_") == 0 && i.indexOf("[format]") > -1) {
        $shortcode.attr("format", settings[i]);
      } else if (!settings[i] && settings[i] !== 0) {
        $shortcode.removeAttr(i);
      } else {
        if (typeof (settings[i]) == "string") {
          // Allow to add "[", "]", "<" and ">" to attributes
          settings[i] = settings[i].replace(/\[/g, "|LS|").replace(/\]/g, "|RS|").replace(/\{/g, '|LF|').replace(/\}/g, "|RF|").replace(/\</g, "|{").replace(/\>/g, "|}").replace(/"/g, "'");
        }
        ''
        $shortcode.attr(i, settings[i]);
      }
    }
  }

  function get_from_cache(html) {
    for (var i in cache_shortcode) {
      if (cache_shortcode[i].raw == html) {
        return cache_shortcode[i].rendered;
      }
    }
    return "";
  }

  function prepare_to_shortcodes($this) {
    // Search for this string in the cache
    //var rendered = get_from_cache(html);
    global_counter++;
    if (typeof ($this.attr("shortcode")) == "undefined") {
      return;
    }
    $this.find("[shortcode!='']").each(function () {
      prepare_to_shortcodes($(this));
    });
    if ($this.attr("class") == "") {
      $this.removeAttr("class");
    }
    var attributes = $this.prop("attributes");
    var attrs_string = "";
    $.each(attributes, function () {
      attrs_string += this.name != "shortcode" ? (" " + this.name + "='" + this.value + "'") : "";
    });
    // Remove required for sortable plugin tags
    if ($this.find("> .visualshortcodes-parent").length) {
      $this.html($this.find("> .visualshortcodes-parent").html());
    }
    // Save in the cache
    //cache_shortcode.push({"raw": html, "rendered": rendered});
    rendered = "<vsb_" + $this.attr("shortcode") + global_counter + attrs_string + ">" + $this.html() + "</vsb_" + $this.attr("shortcode") + global_counter + ">";
    $this.replaceWith(rendered);
  }

  function visualshortcodes_link_text_update($this) {
    if ($("#" + $this.data("id")).prev(".visualshortcodes").length) {
      $this.removeClass("btn-info").addClass("btn-danger").html("<i class='fa fa-ban'></i> " + $this.data("disable-text"));
      $("#" + $this.data("id")).hide();
      $this.show();
    } else {
      $this.removeClass("btn-danger").addClass("btn-info").html("<i class='fa fa-edit'></i> " + $this.data("enable-text"));
      $("#" + $this.data("id")).show();
    }
  }

  function visualshortcodes_save_form($this) {
    $(".visualshortcodes_add_link_active").removeClass("visualshortcodes_add_link_active");
    // Mark the link which have opened the Save Form
    $this.addClass("visualshortcodes_add_link_active");
    // Get the raw code
    var code = $this.closest(".shortcode-processed").outerHTML();
    $("#visualshortcodes-shortcode-save-form textarea").val(code);
    $("#visualshortcodes-shortcode-save-form").dialog("open");
  }

  function visualshortcodes_add_form($this) {
    visualshortcodes_add_overlay();
    var id = $this.parents(".visualshortcodes").next().attr("id");
    var data_format = $(".visualshortcodes_links[data-id='" + id + "']").data("format");
    var data_shortcode = $this.parents(".shortcode-processed").attr("shortcode");
    $(".visualshortcodes_add_link_active").removeClass("visualshortcodes_add_link_active");
    // Mark the link which have opened the Add Form, so will know there to insert the shortcode
    $this.addClass("visualshortcodes_add_link_active");
    $.post(Drupal.settings.basePath + "ajax/visualshortcodes/shortcodes_list", {
      format: data_format,
      shortcode: data_shortcode
    }, function (result) {
      $("#visualshortcodes-shortcode-add-form").html(result);
      $("#visualshortcodes-shortcode-add-form").dialog("open");
      visualshortcodes_remove_overlay();
    });
  }

  function visualshortcodes_add_form_search($this) {
    if (!$this.val()) {
      $this.parents(".visualshortcodes_enabled_list").find(".visualshortcodes_enabled_links button").show();
    } else {
      $this.parents(".visualshortcodes_enabled_list").find(".visualshortcodes_enabled_links button").hide();
      if ($this.data("exactly")) {
        $this.parents(".visualshortcodes_enabled_list").find(".visualshortcodes_enabled_links button[data-title='" + $this.val().toLowerCase() + "']").show();
        $this.removeAttr("data-exactly");
      } else {
        $this.parents(".visualshortcodes_enabled_list").find(".visualshortcodes_enabled_links button[data-title*='" + $this.val().toLowerCase() + "']").show();
      }
    }
  }

  function visualshortcodes_remove_image($this) {
    $this.closest(".form-item").find(".preview-image").html("");
    $this.closest(".form-item").find("input").val("");
    $this.closest("#visualshortcodes-shortcode-settings").find(".fid-old-field").val("");
    $this.hide();
  }

  function visualshortcodes_upload_image($this) {
    globalOptions = {};
    Drupal.media.popups.mediaBrowser(function (mediaFiles) {
      if (mediaFiles.length < 0) {
        return;
      }
      var mediaFile = mediaFiles[0];
      // Set the value of the filefield fid (hidden).
      $this.closest(".form-item").find("input").val(mediaFile.fid);
      $this.closest(".form-item").find(".preview-image").html(mediaFile.preview);
      $this.closest(".form-item").find(".media-remove").show();
    }, globalOptions);
    return false;
  }

  $(document).ready(function () {

    // Dialog Shortcode Settings Form
    $("body").append("<div id = 'visualshortcodes-shortcode-settings' title = '" + Drupal.t("Shortcode Settings") + "'></div>");
    $("#visualshortcodes-shortcode-settings").dialog({
      autoOpen: false,
      width: 1200,
      modal: true,
      position: ["middle", 100],
      open: function () {
        Drupal.attachBehaviors($("#visualshortcodes-shortcode-settings"), Drupal.settings);
        $(this).dialog("option", "position", ["middle", 100]);
      },
      buttons: {
        "Save": function () {
          visualshortcodes_add_overlay();
          // Run change() method so WYSIWYG editors will update textarea values with current values from WYSIWYG layout
          $("#visualshortcodes-shortcode-settings .filter-list").change();
          // Load form values to array
          var settings = _shortcode_form_to_settings($("#visualshortcodes-shortcode-settings"));
          // Load settings to shortcode text and attributes values
          _settings_to_shortcode_attrs(settings, $(".shortcode-opened-settings"));
          // If this is not child element - upload the preview
          if ($(".shortcode-opened-settings > .border-style").hasClass("border-none")) {
            $.post(Drupal.settings.basePath + "ajax/visualshortcodes/ajax_backend_shortcode_preview", {
              attrs: settings,
              shortcode: $(".shortcode-opened-settings").attr("shortcode")
            }, function (data) {
              if ($(".shortcode-opened-settings").attr("shortcode") == "a_saved") {
                var textarea = $(".shortcode-opened-settings").parents(".visualshortcodes").next();
                // Remove processed class from saved shortcode, so new behaviours can be attached
                data = $(data).first().addClass("Drupal-behaviour-please");
                $(".shortcode-opened-settings").replaceWith(data).addClass("Drupal-behaviour-please");
                $(".Drupal-behaviour-please").find(".visualshortcodes-processed").removeClass("visualshortcodes-processed");
                Drupal.attachBehaviors($(".Drupal-behaviour-please"), Drupal.settings);
                $(".Drupal-behaviour-please").removeClass("Drupal-behaviour-please");
                visualshortcodes_save(textarea);
              } else {
                $(".shortcode-opened-settings .backend_preview").remove();
                $(".shortcode-opened-settings .border-style").append(data);
              }
            });
          }
          // Attach Drupal behaviours to updated element to allow scripts made some custom works
          Drupal.attachBehaviors($(".shortcode-opened-settings").parent(), Drupal.settings);
          $(".shortcode-opened-settings").find(".visualshortcodes-settings-links:first").css("display", "block").prepend("<span class = 'saving-info btn btn-xs btn-success'>" + Drupal.t("Saved.") + "</span>")
          setTimeout(function () {
            $(".saving-info").animate({opacity: 0}, 1500, function () {
              $(this).parent(".visualshortcodes-settings-links").removeAttr("style");
              $(this).remove();
            });
            visualshortcodes_remove_overlay();
          }, 1000);
          // We already saved this before
          if ($(".shortcode-opened-settings").attr("shortcode") != "a_saved") {
            // Save updated shortcodes to textarea code
            visualshortcodes_save($(".shortcode-opened-settings").parents(".visualshortcodes").next());
          }
          $(this).dialog("close");
          return false;
        },
        "Delete": function () {
          if (!Drupal.settings.shortcode_bootstrap.confirm_delete || confirm(Drupal.t("Delete shortcode?"))) {
            $(".shortcode-opened-settings").closest(".visualshortcodes").addClass("visualshortcodes_active");
            $(".shortcode-opened-settings").remove();
            visualshortcodes_save($(".visualshortcodes_active").next());
            $(this).dialog("close");
          }
          return false;
        },
        "Cancel": function () {
          $(this).dialog("close");
          return false;
        }
      },
    });

    // Dialog Shortcode Settings Form
    $("body").append("<div id = 'visualshortcodes-shortcode-add-form' title = '" + Drupal.t("Add Shortcode") + "'></div>");
    $("#visualshortcodes-shortcode-add-form").dialog({
      autoOpen: false,
      width: 1000,
      modal: true,
      open: function () {
        Drupal.attachBehaviors($("#visualshortcodes-shortcode-add-form"), Drupal.settings);
      },
      buttons: {
        "Add": function () {
          visualshortcodes_add_overlay();
          var data_shortcode = $(".visualshortcodes_enabled_links .active").data("shortcode");
          $.post(Drupal.settings.basePath + "ajax/visualshortcodes/shortcodes_list_add", {shortcode: data_shortcode}, function (result) {

            $(".visualshortcodes-added-shortcode").removeClass("visualshortcodes-added-shortcode");
            // If this is main ADD button appear at the begining of the layout
            if ($(".visualshortcodes_add_link_active").parent().hasClass("visualshortcodes-main-add")) {
              $(".visualshortcodes_add_link_active").parent().parent().find(".end-layout").before(result);
              $(".visualshortcodes_add_link_active").parent().parent().find("> .shortcode-processed:last").addClass("visualshortcodes-added-shortcode");
            } else {
              $(".visualshortcodes_add_link_active").closest(".shortcode-processed").find(".visualshortcodes-parent:first").append(result);
              $(".visualshortcodes_add_link_active").closest(".shortcode-processed").find(".visualshortcodes-parent:first > .shortcode-processed:last").addClass("visualshortcodes-added-shortcode");
            }
            $(".visualshortcodes_add_link_active").closest(".visualshortcodes").remove("sort-processed");
            // Attach Drupal behaviours to updated element to allow scripts made some custom works
            Drupal.attachBehaviors($(".visualshortcodes_add_link_active").closest(".visualshortcodes"), Drupal.settings);
            // Open the settings form
            $(".visualshortcodes-added-shortcode .shortcode-settings:first").click();
            // Save updates shortcodes to textarea code
            visualshortcodes_save($(".visualshortcodes_add_link_active").closest(".visualshortcodes").next());
            visualshortcodes_remove_overlay();
          });
          $(this).dialog("close");
          // Show the user notification
          $(".visualshortcodes_add_link_active").before("<span class='saving-info btn btn-xs btn-success'>" + Drupal.t("Saved.") + "</span>")
          setTimeout(function () {
            $(".saving-info").animate({opacity: 0}, 1500, function () {
              $(this).remove();
            });
          }, 1000);
        },
        Cancel: function () {
          $(this).dialog("close");
        }
      },
    });

    $("body").append("<div id='visualshortcodes-shortcode-save-form' title='" + Drupal.t("Save Structure") + "'><label>" + Drupal.t("Name:") + "</label><input class='form-control' type='text' name='shortcodes_save'><textarea class='hidden'></textarea></div>");
    $("#visualshortcodes-shortcode-save-form").dialog({
      autoOpen: false,
      width: 1000,
      modal: true,
      open: function () {
        Drupal.attachBehaviors($("#visualshortcodes-shortcode-save-form"), Drupal.settings);
      },
      buttons: {
        "Save": function () {
          $.post(Drupal.settings.basePath + "ajax/visualshortcodes/shortcodes_save", {
            "name": $("#visualshortcodes-shortcode-save-form input").val(),
            "code": $("#visualshortcodes-shortcode-save-form textarea").val()
          });
          $(this).dialog("close");

          // Show the user notification
          $(".visualshortcodes_add_link_active").before("<span class='saving-info btn btn-xs btn-success'>" + Drupal.t("Saved.") + "</span>")
          setTimeout(function () {
            $(".saving-info").animate({opacity: 0}, 1500, function () {
              $(this).remove();
            });
          }, 1000);
        },
        Cancel: function () {
          $(this).dialog("close");
        }
      },
    });
  }); // end doc ready
})(jQuery);
